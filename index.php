<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
?>
<!--Construcción del HTML... -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link href="css/index.css" rel="stylesheet" media="all">
        <title>Bienvenido</title>
    </head>
    <body>
        <h2>INGRESO AL SISTEMA</h2>
        <input name="token" id="token" type="hidden" value='<?php md5(time());?>;'>
        <br/>
        <form>
            <table border="0px" >
                <div id="alerta-error" style="display: none;">
                <tr><td>
                    <label id="labelError"></label><br>
                </tr></td>
                </div>
                <tr>
                    <td align="left" ><label class="formulario">Usuario:&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                    <td><input type="text" name="usuario" id="usuario" maxlength="25" placeholder="Ejemplo: andres23"></td>
                </tr>               
                <tr>
                    <td align="left" ><br/><label class="formulario">Contraseña: &nbsp&nbsp &nbsp&nbsp</label></td>
                    <td><br/><input type="password" name="password" id="password" maxlength="20" placeholder="Ejemplo: *******"></td>
                </tr>                
            </table>
            </br>
            <input type="button" value="Ingresar al sistema" onclick="operacion();"><br/>  
        </form> 
        <a class="link" href="formulario.php">Registrarse</a>     
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
        <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
<!-- Fin HTML -->