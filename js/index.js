function operacion()
{
    //Recolección de los datos
    usuario=$('#usuario').val();
    password=$('#password').val();
    token=$('#token').val();
    //Verificación de nulos
    if(!usuario || !password)
    {
        alert("Usuario y/o contraseña no ingresado, favor ingresar.");
        return;
    }
    
    /*PREPARACIÓN DE AJAX*/  
    $.ajax({
    type: "POST",
    url: "php/iniciar_sesion.php",
    //Var del $_POST[''] : variable atrapada en JS
    data:{
        'usuario':usuario,
        'password':password,
        'token': token
        }
    })
    .done(function(valorSalida)
    {
        if(valorSalida == 1)
        {
            alert('Contraseña no coincide'); //PW no coincide
        }
        else if(valorSalida == 2)
        {
            alert('Usuario no existe'); //Usuario no está
        }
        else if(valorSalida == 3)
        {
            alert('Token invalido interno'); //Token no cuadra
        }
        else if(valorSalida == 0)
        {
            location.href ="main.php"; //Para cuando el acceso se hizo de forma correcta
        }
        else
        {
            alert(valorSalida) //Que muestre el error
        }
    })
    .fail(function(m)
    {
    alert('Hubo un errror'+ m);
    })
}
