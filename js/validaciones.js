function soloLetrasNombre(e)
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "áéíóúabcdefghijklmnñopqrstuvwxyz ";  //notar que no dejará ingreso de espacios. Sólo caracteres
   //especiales = [8,37,39,46];

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}

function soloLetrasNombreNumeros(e)
{
   //Sólo aceptará letras, espacio en blanco y números positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^\w|[ ]|[#]|[-]$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function soloNumerosLetras(e)
{
    //Sólo aceptará números positivos
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^\w+$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function soloNumeros(e)
{
    //Sólo aceptará números positivos
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^\d+$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}
