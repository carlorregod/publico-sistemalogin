function compruebaNombre(nom)
{
	var n=nom.search(/\w+[ ]\w+/g);
	if(n == -1)
		return false;
	else
		return true;
}

function compruebaDomicilio(dom)
{
	var n=dom.search(/\w+[ ][#]?\d+/g); //Ej: Domicilio #8879 o Domicilio 8879
	var num=dom.search(/\d+/);
	if(n == -1 || num == -1)
		return false;
	else
		return true;
}

function compruebaUsuario(alias, mensaje)
{
    //Consultar si el caracter tiene dígitos
    var regex_num = /\d+/;
    //Consultar si el caracter tiene letras
    var regex_txt= /([a-zA-Z])+/;
    if(alias.search(regex_num) == -1 || alias.search(regex_txt) == -1)
    {
        alert(mensaje+" a registrar debe poseer al menos, una letra y un número. Vuelva a ingresar.")
        return 1;
    }

    //Consulta si la cadena posee al menos, 5 líneas de extensión
    if(alias.length<=5)
    {
        alert(mensaje+" a registrar debe poseer más de 5 caracteres. Número y letras obligatorio.")
        return 1;
    }
}

function operacion()
{
	//Recolectar datos
	var nombre=$('#nombre').val();
	var domicilio=$('#domicilio').val();
	var edad=$('#edad').val();
	var usuario=$('#usuario').val();
	var password=$('#password').val();
	var password_confirm=$('#password_confirm').val();

	//Evitar campos vacíos
	if(!nombre || !domicilio || !edad || !usuario || !password || !password_confirm)
	{
		alert('Debe completar todos los datos del formulario');
		return;
	}

	//Comprobar que se ingresa aunque sea un nombre+apellido
	if(!compruebaNombre(nombre))
	{
		alert('Ingrese al menos un nombre y apellido, separado por un espacio');
		return;
	}

	//Comprobar que se ingresa domicilio un espacio Y un dígito (Ej: platon 21)
	if(!compruebaDomicilio(domicilio))
	{
		alert('Ingrese al menos un domicilio, separado por un espacio');
		return;
	}

	//Comprobar edad. Se fijará patrón de que no sea mayor a 100 años
	if(edad>=100)
	{
		alert('La edad ingresada debe comprender entre 1 y 99 años');
		return;
	}

	//Usuario debe poseer al menos 5 caracteres, una letra y un número como mínimo
	if(compruebaUsuario(usuario, 'Usuario') ==1)
		return;


	//Passwords deben ser iguales
	if(password != password_confirm)
	{
		alert('Contraseñas no coinciden');
		return;
	}

	//Passwords deben ser de mínimo 6 caracteres, poseer una letra y/o un número
	if(compruebaUsuario(password, 'Contraseña') ==1)
		return;

	//Preparación de AJAX
	$.ajax({
		type: 'POST',
		url: 'php/enviar_formulario.php',
		data : { 
			'nombre': nombre,
			'domicilio': domicilio,
			'edad': edad,
			'usuario': usuario,
			'password': password,
			 }
	})
	.done(function(opcion){
		if(opcion == 0)
		{
			alert('Ingreso exitoso');
			limpiar();
		}
		else if(opcion == 1)
			alert('Usuario ya existe, favor especificar otro');
		else
			alert('Error. Vuelva a intentarlo más adelante '+opcion )
	})
	.fail(function(mensaje){ alert('Error!!!' +mensaje);})
}

function limpiar()
{
	$('#nombre').val('');
	$('#domicilio').val('');
	$('#edad').val('');
	$('#usuario').val('');
	$('#password').val('');
	$('#password_confirm').val('');
}