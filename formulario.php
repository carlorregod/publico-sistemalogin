<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
?>
<!--Construcción del HTML... -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/index.css">
        <title>Nuevo usuario</title>
    </head>
    <body>
        <h2>FORMULARIO NUEVO USUARIO</h2>
        <br/>
        <form>
            <table border="0px" >
                <div id="alerta-error" style="display: none;">
                <tr><td>
                    <label id="labelError"></label><br>
                </tr></td>
                </div>
                <tr>
                    <td align="left" ><label class="formulario">Nombre completo:&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                    <td><input type="text" name="nombre" id="nombre" maxlength="25" placeholder="Ejemplo: Andrés Salas" onkeypress="return soloLetrasNombre(event)"></td>
                </tr>   
                <tr>
                    <td align="left" ><label class="formulario">Domicilio:&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                    <td><input type="text" name="domicilio" id="domicilio" maxlength="50" placeholder="Ejemplo: Avenida Copiapó 28 Isla de Maipo" onkeypress="return soloLetrasNombreNumeros(event)"></td>
                </tr>  
                <tr>
                    <td align="left" ><label class="formulario">Edad:&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                    <td><input type="number" min="1" max="99" name="edad" id="edad" maxlength="2" ;"placeholder="Ejemplo: 18" onkeypress="return soloNumeros(event)"></td>
                </tr>  
                <tr>
                    <td align="left" ><label class="formulario">Usuario:&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                    <td><br/><input type="text" name="usuario" id="usuario" maxlength="25" placeholder="Ejemplo: andres23" onkeypress="return soloNumerosLetras(event)"></td>
                </tr>               
                <tr>
                    <td align="left" ><label class="formulario">Contraseña:</label></td>
                    <td><input type="password" name="password" id="password" maxlength="20" placeholder="Ejemplo: *******"></td>
                </tr>
                <tr>
                    <td align="left" ><label class="formulario">Confirmar Contraseña:</label></td>
                    <td><input type="password" name="password_confirm" id="password_confirm" maxlength="20"></td>
                </tr>                 
            </table>
            <br>
            <input type="button" value="Registrarse" onclick="operacion();"><br/>  
        </form> 
        <a class="link" href="./">Volver</a>     
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
        <script type="text/javascript" src="js/validaciones.js"></script>
        <script type="text/javascript" src="js/formulario.js"></script>
        
    </body>
</html>
<!-- Fin HTML -->