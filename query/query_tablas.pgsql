/**Compendio de tablas y compleción de ellas para los combobox**/
--CREACION DE TABLAS Y ALGUNOS VALORES DE REFERENICIA

create table usuario1(
id serial not null,
nombre varchar not null,
domicilio varchar not null,
edad int,
usuario varchar not null,
paswd text not null,
token_remember text,
primary key (id)
);

alter table usuario1
   add constraint UQ_usuario
   unique (usuario);