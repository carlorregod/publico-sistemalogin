<?php
require 'conexion.php';

function setUsuario()
{
    $pgsql = getConn();
    //Traer los datos desde el AJAX
    $nombre = pg_escape_string($_POST['nombre']);
    $domicilio = pg_escape_string($_POST['domicilio']);
    $edad = pg_escape_string($_POST['edad']);
    $usuario = pg_escape_string($_POST['usuario']);
    $password = pg_escape_string($_POST['password']);
    //Preguntarnos si el usuario está de antemano en la BD
    $query = "SELECT usuario FROM usuario1 WHERE usuario='$usuario'";
    if(pg_fetch_object(pg_query($pgsql, $query))->usuario == $usuario)
        return 1; //Futuro mensaje de duplicidad de usuario
    else
    {
        //Proceso de generar el registro
        //Encriptar el password
        $password = password_hash($password, PASSWORD_DEFAULT);
        //Ejecutando la query
        $query = "INSERT INTO usuario1 (nombre, domicilio, edad, usuario, paswd) VALUES
        ('$nombre','$domicilio','$edad','$usuario','$password')";
        pg_query($pgsql, $query) or die('Falló la query: ' . pg_last_error());;
        return 0; //Futuro mensaje de creación exitosa
    }
   
};

echo setUsuario();