<?php
require 'conexion.php';

function ingresoSistema()
{
    $pgsql = getConn();
    //Captura de usuario
    $usuario=pg_escape_string($_POST['usuario']); 
    $password=pg_escape_string($_POST['password']);
    //Tratamiento del token
        
    //Haciendo la query
    $query = "SELECT * FROM usuario1 WHERE usuario='$usuario'";
    $result = pg_query($pgsql, $query) or die('Falló la query: ' . pg_last_error());
    $resultado=pg_fetch_all($result); //Arreglo de ocurrencias

    //¿Existe el usuario en la BD?
    if($usuario != $resultado[0]['usuario'])
        return 2;

    //Autenticidad por el password    
    $pass = $resultado[0]['paswd'];
    if(!password_verify($password, $pass))
        return 1; 

    //Almacenamiento del token por inicio de sesión
    $token = crypt($_POST['token']);
    $query="UPDATE usuario1 SET token_remember ='$token' WHERE usuario='$usuario'";
    pg_query($pgsql,$query);

    //Iniciando Sesión ya que todo anda bien
    session_start();
    $_SESSION['usuario']=$usuario;
    $_SESSION['nombre']=$resultado[0]['nombre'];
    $_SESSION['token'] = $token;

    return 0;

}

echo ingresoSistema();


/*OTRA FORMA DE CONSULTAR, EN ESTE CASO ES VIABLE PUES SOLO SE TIENE 1 OCURRENCIA*/
/*
$resultado=pg_fetch_object($result); //Arreglo de ocurrencias
//¿Existe el usuario en la BD?
if($usuario != $resultado->usuario)
    return 2;
//Autenticidad por el password    
$pass = $resultado->paswd;
if(!password_verify($password, $pass))
    return 1; 
*/