<?php
require_once 'conexion.php';

function mantenerConexion()
{
    //Gestión de errores
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    //Consulta en BD del token guardado cuando se inició sesión
    $pgsql=getConn();
    session_start();
    $usuario=$_SESSION['usuario'];
    $query="SELECT * FROM usuario1 WHERE usuario='$usuario'";
    $resultado_=pg_query($pgsql,$query);
    $resultado=pg_fetch_object($resultado_);
    $token=$resultado->token_remember;    
    if(!isset($_SESSION['usuario'])) 
    {
        header("Location: ./index.php");
        exit();
    }
    
    if($_SESSION["token"] != $token ) 
    {
        header("Location: ./index.php");
        echo '<p>Token no validado</p>'; //Comentar esta linea en producción
        exit();
    } 
    return;
}
