<?php
require 'php/mantener_sesion.php';
mantenerConexion();
?>

<!--Construcción del HTML...Pantalla del MAIN -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link href="css/index.css" rel="stylesheet" media="all">
        <title>Sistema</title>
    </head>
    <body>
        <h2>BIENVENIDO(A) <?php echo strtoupper($_SESSION['nombre']);?></h2>
        <br/>
        <form>
            <table border="0px" >
                <div id="alerta-error" style="display: none;">
                <tr><td>
                    <label id="labelError"></label><br>
                </tr></td>
                </div>
                <tr>
                    <td align="left" ><label>Ingreso Exitoso :) !&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                </tr>                              
            </table>
            <br>
            <input type="button" value="Salir del sistema" onclick="location.href='php/cerrar_sesion.php'"><br/>  
        </form>      
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
        <script type="text/javascript" src="js/index.js"></script>

    </body>
</html>
<!-- Fin HTML -->
